import java.util.ArrayList;
import java.util.Scanner;

public class MainClass {
    public static void main(String[] args) {

        byte Ulangi = 1;
        ArrayList<Film> films = new ArrayList<Film>();
        ArrayList<Musik> musik = new ArrayList<Musik>();
        Scanner kScanner = new Scanner(System.in);

        while (Ulangi == 1) {

            String judul, publisher;
            Kategori kategori;
            int stok;

            System.out.printf(
                    "\n===============================\nPilih Opsi :\n1. Tampil Semua DVD\n2. Tampil DVD Film\n3. Tampil DVD Musik\n4. Entry DVD Film\n5. Entry DVD Musik\n99. Exit\n: ");

            byte pilihan = kScanner.nextByte();

            switch (pilihan) {
                case 1:
                    System.out.println("List DVD Film :");
                    films.forEach((n) -> n.print());

                    System.out.println("\nList DVD Musik :");
                    musik.forEach((n) -> n.print());
                    break;

                case 2:
                    System.out.println("List DVD Film :");
                    films.forEach((n) -> n.print());
                    break;

                case 3:
                    System.out.println("\nList DVD Musik :");
                    musik.forEach((n) -> n.print());
                    break;

                case 4:
                    ArrayList<String> pemeran = new ArrayList<String>();
                    String sutradara;
                    int jmlPemeran;

                    System.out.println("\n = Entry DVD FILM =");
                    System.out.printf("Masukan Judul\t\t:");
                    judul = kScanner.next();
                    System.out.printf("Masukan Jumlah Pemeran\t:");
                    jmlPemeran = kScanner.nextInt();

                    for (int i = 0; i < jmlPemeran; i++) {
                        System.out.print("Masukan Pemeran " + (i + 1) + "\t:");
                        pemeran.add(kScanner.next());
                    }

                    System.out.printf("Masukan Sutradara\t:");
                    sutradara = kScanner.next();

                    System.out.printf("Masukan Publisher\t:");
                    publisher = kScanner.next();

                    System.out.printf(
                            "Masukan Kategori :\n\t1. SU (Semua Umur)\n\t2. D (Dewasa)\n\t3. R (Remaja)\n\t4. A (Anak-Anak)\n:");
                    pilihan = kScanner.nextByte();

                    switch (pilihan) {
                        case 1:
                            kategori = Kategori.SU;
                            break;

                        case 2:
                            kategori = Kategori.D;
                            break;

                        case 3:
                            kategori = Kategori.R;
                            break;

                        case 4:
                            kategori = Kategori.A;
                            break;

                        default:
                            kategori = Kategori.SU;
                            break;
                    }

                    System.out.printf("Masukan Stok\t\t:");
                    stok = kScanner.nextInt();

                    films.add(new Film(judul, pemeran, sutradara, publisher, kategori, stok));
                    break;

                case 5:
                    String penyanyi;
                    String produser;
                    ArrayList<String> topHits = new ArrayList<String>();
                    int jmlHits;

                    System.out.println("\n = Entry DVD MUSIK =");
                    System.out.printf("Masukan Judul\t\t\t:");
                    judul = kScanner.next();
                    System.out.printf("Masukan Penyanyi\t\t:");
                    penyanyi = kScanner.next();
                    System.out.printf("Masukan Produser\t\t:");
                    produser = kScanner.next();
                    System.out.printf("Masukan Publisher\t\t:");
                    publisher = kScanner.next();
                    System.out.printf("Masukan Jumlah lagu Top Hits\t:");
                    jmlHits = kScanner.nextInt();

                    for (int i = 0; i < jmlHits; i++) {
                        System.out.printf("Masukan judul lagu hits %d\t:", i + 1);
                        topHits.add(kScanner.next());
                    }

                    System.out.printf(
                            "Masukan Kategori :\n\t1. C (Classic)\n\t2. J (Jazz)\n\t3. P (Pop)\n\t4. R (Rock)\n\t5. O (Other)\n:");
                    pilihan = kScanner.nextByte();

                    switch (pilihan) {
                        case 1:
                            kategori = Kategori.C;
                            break;

                        case 2:
                            kategori = Kategori.J;
                            break;

                        case 3:
                            kategori = Kategori.P;
                            break;

                        case 4:
                            kategori = Kategori.R;
                            break;

                        default:
                            kategori = Kategori.O;
                            break;
                    }

                    System.out.printf("Masukan Stok\t\t\t:");
                    stok = kScanner.nextInt();

                    musik.add(new Musik(judul, penyanyi, produser, publisher, topHits, kategori, stok));
                    break;

                default:
                    Ulangi = 0;
                    break;
            }

            System.out.println("===============================\n");
        }
    }
}
