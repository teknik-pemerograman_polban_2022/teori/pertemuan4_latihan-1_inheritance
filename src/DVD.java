enum Kategori {
    C {
        public String toString() {
            return "Classic";
        }
    },
    J {
        public String toString() {
            return "Jazz";
        }
    },
    P {
        public String toString() {
            return "Pop";
        }
    },
    R {
        public String toString() {
            return "Rock";
        }
    },
    O {
        public String toString() {
            return "Other";
        }
    },
    SU {
        public String toString() {
            return "Semua Umur";
        }
    },
    D {
        public String toString() {
            return "Dewasa";
        }
    },
    R1 {
        public String toString() {
            return "Remaja";
        }
    },
    A {
        public String toString() {
            return "Anak-anak";
        }
    }
}

public class DVD {
    private String judul;
    private String publisher;
    private Kategori kategori;
    private int stok = 0;

    public String getJudul() {
        return this.judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getPublisher() {
        return this.publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public Kategori getKategori() {
        return this.kategori;
    }

    public void setKategori(Kategori kategori) {
        this.kategori = kategori;
    }

    public int getStok() {
        return this.stok;
    }

    public void setStok(int stok) {
        this.stok = stok;
    }

    public void print() {
        System.out.printf("Judul\t\t= %s\nPublisher\t= %s\nKategori\t= %s\nStok\t\t= %d", judul, publisher, kategori,
                stok);
    }

}
