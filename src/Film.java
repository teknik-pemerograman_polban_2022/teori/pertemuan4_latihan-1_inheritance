import java.util.ArrayList;

public class Film extends DVD {
    ArrayList<String> pemeran = new ArrayList<String>();
    private String sutradara;

    public Film(String JudulFilm, ArrayList<String> PemeranFilm, String Sutradara, String Publisher, Kategori Kategori,
            int Stok) {
        this.setJudul(JudulFilm);
        setPemeran(PemeranFilm);
        setSutradara(Sutradara);
        this.setPublisher(Publisher);
        this.setKategori(Kategori);
        this.setStok(Stok);
    }

    public ArrayList<String> getPemeran() {
        return this.pemeran;
    }

    public void setPemeran(ArrayList<String> Pemeran) {
        for (String string : Pemeran) {
            this.pemeran.add(string);
        }
    }

    public String getSutradara() {
        return this.sutradara;
    }

    public void setSutradara(String sutradara) {
        this.sutradara = sutradara;
    }

    public void print() {
        super.print();
        System.out.printf("\nPemeran\t\t= ");
        System.out.print(pemeran);
        System.out.printf("\nSutradara\t= %s\n",
                sutradara);
    }

}
