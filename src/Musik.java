import java.util.ArrayList;

public class Musik extends DVD {
    private String penyanyi;
    private String produser;
    ArrayList<String> TopHits = new ArrayList<String>();

    public Musik(String JudulMusik, String Penyanyi, String Produser, String Publisher, ArrayList<String> Hits,
            Kategori Kategori,
            int Stok) {
        this.setJudul(JudulMusik);
        setPenyanyi(Penyanyi);
        setProduser(Produser);
        this.setPublisher(Publisher);
        this.setTopHits(Hits);
        this.setKategori(Kategori);
        this.setStok(Stok);
    }

    public ArrayList<String> getTopHits() {
        return this.TopHits;
    }

    public void setTopHits(ArrayList<String> TopHits) {
        for (String string : TopHits) {
            this.TopHits.add(string);
        }
    }

    public String getPenyanyi() {
        return this.penyanyi;
    }

    public void setPenyanyi(String penyanyi) {
        this.penyanyi = penyanyi;
    }

    public String getProduser() {
        return this.produser;
    }

    public void setProduser(String produser) {
        this.produser = produser;
    }

    public void print() {
        super.print();

        System.out.printf("\nPenyanyi\t= %s", this.penyanyi);
        System.out.printf("\nProduser\t= %s", this.produser);
        System.out.printf("\nTop Hits\t= ");
        System.out.print(TopHits);
        System.out.println("\n");
    }

}
